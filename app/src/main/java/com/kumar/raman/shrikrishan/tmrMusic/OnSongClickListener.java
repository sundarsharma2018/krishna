package com.kumar.raman.shrikrishan.tmrMusic;

/**
 * Created by Sachin Rathore on 7/21/2019.
 */

public interface OnSongClickListener {
    void onSongClick();

    void updateSong();
}
