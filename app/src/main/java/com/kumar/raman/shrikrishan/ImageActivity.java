package com.kumar.raman.shrikrishan;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.VolleyError;
import com.kumar.raman.shrikrishan.Adapter.ImgViewHolder;
import com.kumar.raman.shrikrishan.Pojo.ImagesData;
import com.kumar.raman.shrikrishan.Pojo.UploadInfo;
import com.kumar.raman.shrikrishan.networking.ImageFilePath;
import com.kumar.raman.shrikrishan.networking.NetworkingCallbackInterface;
import com.kumar.raman.shrikrishan.networking.RequestHandler;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class ImageActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    private FirebaseRecyclerAdapter<UploadInfo, ImgViewHolder> mAdapter;
    private List<ImagesData> imagesList = new ArrayList<>();
    Spinner selctOptions;
    LinearLayout uploadImage;
    ImageView imagePreview;
    private static final int PERMISSION_REQUEST_CAMERA = 0;
    private static final int PERMISSION_REQUEST_AUDIO = 5;
    private static final int PERMISSION_REQUEST_STORAGE = 2;
    private static final int REQUEST_CAMERA = 1000;
    private static final int SELECT_FILE = 2000;
    static final int CAMERA_PIC_REQUEST = 1000;
    static final int GALLERY_PIC_REQUEST = 2000;
    static final int VIDEO_CAPTURE = 4000;
    String mCurrentPhotoPath;
    public ArrayList<String> imageURLList = new ArrayList<>();
    ProgressDialog progress;
    private StorageReference mStorageRef;
    private DatabaseReference mDataReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audio);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        mStorageRef = FirebaseStorage.getInstance().getReference();
        mDataReference = FirebaseDatabase.getInstance().getReference("images");

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        uploadImage=(LinearLayout) findViewById(R.id.uploadImage);
        setTitle("Wallpaper");




        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        //recyclerView.setAdapter(mAdapter);
        if(MainActivity.loginFlag){
            uploadImage.setVisibility(View.VISIBLE);
        }else{
            uploadImage.setVisibility(View.GONE);
        }


        uploadImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createDailogBox();
            }
        });
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        progress = new ProgressDialog(this);
        if(isNetworkConnected()) {
            showProgressDialog();
        }else {
            Toast.makeText(getApplicationContext(), "Internet connection not available",Toast.LENGTH_SHORT).show();
        }
        Query query = mDataReference.limitToLast(12);

        mAdapter = new FirebaseRecyclerAdapter<UploadInfo, ImgViewHolder>(
                UploadInfo.class, R.layout.item_image, ImgViewHolder.class, query) {
            @Override
            protected void populateViewHolder(ImgViewHolder viewHolder, UploadInfo model, int position) {
                progress.dismiss();
                //viewHolder.nameView.setText(model.name);
              final String url=model.url;
                Picasso.with(ImageActivity.this)
                        .load(model.url)
                        .placeholder(R.drawable.placeholder_image)
                        .error(R.drawable.common_google_signin_btn_icon_dark)
                        .into(viewHolder.imageView);

                viewHolder.imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i=new Intent(ImageActivity.this,FullScrenActivity.class);
                        i.putExtra("url",url);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);

                    }
                });
            }

        };
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        progress.dismiss();
        finish();
        mAdapter.cleanup();
    }

    public  void showCustomDialog(final Bitmap bitmap) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.success_error_view1);
     ImageView imagePreview=(ImageView)dialog.findViewById(R.id.imagePreview);

        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        Button okButton = (Button) dialog.findViewById(R.id.okButton);
        Button cancelButton=(Button) dialog.findViewById(R.id.cancelButton);
        imagePreview.setImageBitmap(bitmap);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                if(isNetworkConnected()) {
                    File newFile = getFile(bitmap);
                    progress.setMessage("Image Uploading");
                    progress.show();
                    UploadToFirebase(newFile);
                    dialog.dismiss();
                }else{
                    dialog.dismiss();
                    progress.dismiss();
                    Toast.makeText(getApplicationContext(), "Internet connection not available",Toast.LENGTH_SHORT).show();
                }
            }
        });
        dialog.show();
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


    }
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }
    public void createDailogBox() {
        CharSequence colors[] = new CharSequence[]{"Camera",
                "Gallery"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select an Options");
        builder.setItems(colors, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int pos) {


                if (pos == 0) {
                    showCameraPreview();
                } else if (pos == 1) {
                    showStoragereview();
                }
            }
        });
        builder.show();

    }

    private void showCameraPreview() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) ==
                PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_GRANTED) {
            // Permission is already available, start camera preview
            startNewCamera();
           // startNewCamera();

        } else {
            // Permission is missing and must be requested.
            requestCameraPermission();
        }
    }
public  void startNewCamera(){
    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

    if (intent.resolveActivity(getPackageManager()) != null) {
        startActivityForResult(intent, 7777);
    }
}
    private void requestCameraPermission() {
        // Permission has not been granted and must be requested.
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                android.Manifest.permission.CAMERA) && ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            requestPermissions(new String[]{android.Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSION_REQUEST_CAMERA);

        } else {
            requestPermissions(new String[]{android.Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSION_REQUEST_CAMERA);
        }
    }
    public void startCamera() {
        File f = null;
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        try {
            f = setUpPhotoFile();
            mCurrentPhotoPath = f.getAbsolutePath();
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
        } catch (IOException e) {
            e.printStackTrace();
            f = null;
            mCurrentPhotoPath = null;
        }
        startActivityForResult(takePictureIntent, 777);
    }

    private File setUpPhotoFile() throws IOException {

        File f = createImageFile();
        mCurrentPhotoPath = f.getAbsolutePath();

        return f;
    }
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File

            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.example.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, 777);
            }
        }
    }
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }
    private void showStoragereview() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_GRANTED) {
            // Permission is already available, start camera preview
            startGallery();
        } else {
            // Permission is missing and must be requested.
            requestStoragePermission();
        }
    }

    private void requestStoragePermission() {
        // Permission has not been granted and must be requested.
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSION_REQUEST_STORAGE);

        } else {
            requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSION_REQUEST_STORAGE);
        }
    }

    public void startGallery() {
        Intent intent = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(
                Intent.createChooser(intent, "Select File"),
                SELECT_FILE);
    }
    private void writeNewImageInfoToDB(String name, String url) {
        UploadInfo info = new UploadInfo(name, url);

        String key = mDataReference.push().getKey();
        mDataReference.child(key).setValue(info);
        progress.dismiss();

    }
   public void UploadToFirebase(File file){

       //Uri file = Uri.fromFile(new File("path/to/images/rivers.jpg"));
       final int random = new Random().nextInt(61) + 20; // [0, 60] + 20 => [20, 80]

       StorageReference riversRef = mStorageRef.child("images/"+ "Image_"+random+".jpg");
       riversRef.putFile(Uri.fromFile(file))
               .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                   @Override
                   public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                       // Get a URL to the uploaded content

                       final int random = new Random().nextInt(61) + 20; // [0, 60] + 20 => [20, 80]

                       String name = taskSnapshot.getMetadata().getName();
                       String url = taskSnapshot.getDownloadUrl().toString();
                       writeNewImageInfoToDB(name, url);

                   }
               })
               .addOnFailureListener(new OnFailureListener() {
                   @Override
                   public void onFailure(@NonNull Exception exception) {
progress.dismiss();
                       Toast.makeText(getApplicationContext(), "Failed to upload",Toast.LENGTH_SHORT).show();
// ...
                   }
               });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        Bitmap bm = null;

        if(requestCode==7777){
//             if(mCurrentPhotoPath!=null)
//            bm = getBitmapWithOrientation(mCurrentPhotoPath, 768);
           if(data!=null) {
               Bundle extras = data.getExtras();
               Bitmap imageBitmap = (Bitmap) extras.get("data");
               //mImageLabel.setImageBitmap(imageBitmap);
               //encodeBitmapAndSaveToFirebase(imageBitmap);

               showCustomDialog(imageBitmap);
           }
        }
       else if (requestCode == GALLERY_PIC_REQUEST && resultCode == RESULT_OK
                && null != data) {

            InputStream is = null;
            try {
                String realPath = ImageFilePath.getPath(this, data.getData());
                Bitmap bitmap = getBitmapWithOrientation(realPath, 768);
                showCustomDialog(bitmap);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }
    public static Bitmap getBitmapWithOrientation(String path, int newSize) {
        Bitmap myBitmap = BitmapFactory.decodeFile(path);

        try {
            ExifInterface exif = new ExifInterface(path);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
            } else if (orientation == 3) {
                matrix.postRotate(180);
            } else if (orientation == 8) {
                matrix.postRotate(270);
            }
            int width = myBitmap.getWidth();
            int height = myBitmap.getHeight();
            if (newSize != 0) {
                if (height > width) {
                    if (width > newSize) {
                        float ratio = (float) height / (float) width;
                        width = newSize;
                        height = (int) ((float) width * ratio);
                    }
                } else {
                    if (height > newSize) {
                        float ratio = (float) width / (float) height;
                        height = newSize;
                        width = (int) ((float) height * ratio);
                    }
                }
            }
            myBitmap = Bitmap.createScaledBitmap(myBitmap, width, height, false);
            myBitmap = Bitmap.createBitmap(myBitmap, 0, 0, myBitmap.getWidth(), myBitmap.getHeight(), matrix, true); // rotating bitmap
        } catch (Exception e) {
            e.printStackTrace();
        }
        return myBitmap;
    }
    public File getFile(Bitmap bm) {
        // Find the SD Card path
        File filepath = Environment.getExternalStorageDirectory();

        // Create a new folder in SD Card
        // getAbsolutePath for sd card
        File dir;
        dir = new File(filepath.getAbsolutePath() + "/MTCollect/images/others");


        dir.mkdirs();
        // Create a name for the saved image
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String currentDateandTime = sdf.format(new Date());

        String saveImageName = "";
        saveImageName = "" + (imageURLList.size() + 1) + ".jpg";


        File file = new File(dir, saveImageName);

        // Show a toast message on successful save
        /*Toast.makeText(getApplicationContext(), "Image Saved to SD Card",
                Toast.LENGTH_SHORT).show();*/
        try {

            OutputStream output = new FileOutputStream(file);

            //  Bitmap bm = cropBorderFromBitmap(finalImg);

            // Compress into png format image from 0% - 100%
            bm.compress(Bitmap.CompressFormat.JPEG, 50, output);
            output.flush();
            output.close();

            refreshGallery(file);
            //	MediaScannerConnection.scanFile(this, new String[] { dir.getPath() }, new String[] { "image/jpeg" }, null);


        } catch (Exception e) {
            e.printStackTrace();
        }
        return file;

    }
    private void refreshGallery(File file) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        mediaScanIntent.setData(Uri.fromFile(file));
        sendBroadcast(mediaScanIntent);
    }

    public void getAllImages() {

        JSONObject json =new JSONObject();
        showProgressDialog();

        RequestHandler.getAllImages(this,json, new NetworkingCallbackInterface() {
            @Override
            public void onSuccess(NetworkResponse response, boolean fromCache) {
                System.out.print("response........"+response);
                try {
                    JSONArray jsonArray=new JSONArray(response);
                    System.out.print("jsonArrayresponse........"+jsonArray);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onSuccess(String response, boolean fromCache) {
                System.out.print("response........"+response);
                progress.dismiss();
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("posts");
                    ImagesData content=new ImagesData();
                    for(int i=0;i<jsonArray.length();i++){
                        JSONObject json= jsonArray.getJSONObject(i);
                        JSONArray arrayAttachment=json.getJSONArray("attachments");
                        for(int j=0;j<arrayAttachment.length();j++) {
                            JSONObject attachJson= arrayAttachment.getJSONObject(j);
                            JSONObject imagesJson=attachJson.getJSONObject("images");
                            content=new ImagesData();
                               JSONObject fullJson= imagesJson.getJSONObject("full");
                               JSONObject thumbnailJson= imagesJson.getJSONObject("thumbnail");
                                if(imagesJson.has("full")){
                                  //  JSONObject newJson= imagesJson.getJSONObject("full");
                                    content.setFullImage(fullJson.getString("url"));
                                }
                                if(imagesJson.has("thumbnail")){
                                   // JSONObject thumbnailJson= imagesJson.getJSONObject("thumbnail");
                                    content.setThumbnal(thumbnailJson.getString("url"));
                                }

                            imagesList.add(content);
                        }

                    }
//                    mAdapter = new ImagesAdapter(imagesList,getApplicationContext(),getSupportFragmentManager()
//                            .beginTransaction());
                    recyclerView.setAdapter(mAdapter);
                    System.out.print("jsonObjectArrayresponse........"+jsonArray);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(VolleyError error) {
            }

            @Override
            public void onNetworkFailure(String error) {

            }
        });
    }
    public void showProgressDialog(){
        progress.setTitle("Loading");
        progress.setMessage("Wait while loading...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();
       //Todo getdata
    }
}
