package com.kumar.raman.shrikrishan.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kumar.raman.shrikrishan.FullScreenTextActivity;
import com.kumar.raman.shrikrishan.Pojo.AratiData;
import com.kumar.raman.shrikrishan.R;

import java.util.List;

/**
 * Created by mann on 20/2/18.
 */

public class GeetaAdapter extends RecyclerView.Adapter<GeetaAdapter.MyViewHolder>{
    FragmentTransaction ft;
    private List<AratiData> geetaList;


    Context mContext;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView textview1;
        LinearLayout mainLay;
        private GeetaAdapter.ItemClickListener clickListener;
        public MyViewHolder(View view) {
            super(view);

            textview1 = (TextView) view.findViewById(R.id.textview1);
            mainLay=(LinearLayout)view.findViewById(R.id.mainLay);
        }

        public void setClickListener(GeetaAdapter.ItemClickListener itemClickListener) {
            this.clickListener = itemClickListener;
        }


    }


    public GeetaAdapter(List<AratiData> arrayList, Context context, FragmentTransaction ft) {
        this.geetaList = arrayList;
        this.mContext = context;
        this.ft=ft;
    }

    @Override
    public GeetaAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.text_row, parent, false);
        return new GeetaAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(GeetaAdapter.MyViewHolder holder, final int position) {


        final String description=geetaList.get(position).getContent();
        String title=geetaList.get(position).getTitle();

        holder.textview1.setText(title);
        holder.mainLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(mContext,FullScreenTextActivity.class);
                i.putExtra("title",geetaList.get(position).getTitle());
                i.putExtra("content",geetaList.get(position).getContent());
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(i);
            }
        });

    }

    @Override
    public int getItemCount() {
        return geetaList.size();
    }
    public interface ItemClickListener {
        void onClick(View view, int position, boolean isLongClick);
    }


}