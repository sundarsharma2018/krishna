package com.kumar.raman.shrikrishan.tmrMusic.fragments;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.kumar.raman.shrikrishan.R;
import com.kumar.raman.shrikrishan.tmrMusic.PlayBackManagerForFragment;
import com.kumar.raman.shrikrishan.tmrMusic.TmrBhajanAdapter;
import com.kumar.raman.shrikrishan.tmrMusic.SongService;
import com.kumar.raman.shrikrishan.tmrMusic.helpers.BitmapPalette;
import com.kumar.raman.shrikrishan.tmrMusic.helpers.PlayPauseView;
import com.kumar.raman.shrikrishan.tmrMusic.slidingtabhelper.SlidingTabLayout;
import com.kumar.raman.shrikrishan.tmrMusic.slidingtabhelper.ViewPagerAdapter;
import com.kumar.raman.shrikrishan.tmrMusic.slidinguppanelhelper.SlidingUpPanelLayout;

import java.util.ArrayList;
import java.util.HashMap;

import static com.kumar.raman.shrikrishan.tmrMusic.helpers.BitmapPalette.blurredBitmap;
import static com.kumar.raman.shrikrishan.tmrMusic.helpers.BitmapPalette.mediumBitmap;
import static com.kumar.raman.shrikrishan.tmrMusic.helpers.BitmapPalette.smallBitmap;

/**
 * Created by Sachin Rathore on 7/27/2019.
 */

public class TmrBhajanFragment extends Fragment implements View.OnClickListener {
    Context mContext;
    public static final String SONG_TITLE = "songTitle";
    public static final String DISPLAY_NAME = "displayName";
    public static final String SONG_ID = "songID";
    public static final String SONG_PATH = "songPath";
    public static final String ALBUM_NAME = "albumName";
    public static final String ARTIST_NAME = "artistName";
    public static final String SONG_DURATION = "songDuration";
    public static final String SONG_POS = "songPosInList";
    public static final String SONG_PROGRESS = "songProgress";
    private Toolbar toolbar;
    private ListView lv_songslist;
    private TmrBhajanAdapter mAllSongsListAdapter;
    private SlidingUpPanelLayout mLayout;
    private ImageView songAlbumbg, img_bottom_slideone, img_bottom_slidetwo,
            imgbtn_backward, imgbtn_forward, ivListBG;
    private PlayPauseView btn_playpause, btn_playpausePanel;
    private TextView txt_timeprogress, txt_timetotal, txt_playesongname,
            txt_songartistname, txt_playesongname_slidetoptwo, txt_songartistname_slidetoptwo;

    private RelativeLayout slidepanelchildtwo_topviewone, slidepanelchildtwo_topviewtwo;
    private LinearLayout llBottomLayout;
    private boolean isExpand = false;
    private SharedPreferences sharedPref;
    private PlayBackManagerForFragment playBackManagerForFragment;
    private SeekBar seekBar;
    Thread thread;
    ViewPager pager;
    ViewPagerAdapter adapter;
    SlidingTabLayout tabs;
    CharSequence Titles[] = {"All Songs", "Playlists", "Favourites"};
    int Numboftabs = 3;
    public static boolean shouldContinue = false;
    String type = "ringtone";
    ProgressDialog progress;
    private LinearLayout llProgress;
    private ProgressBar progressBar;
    ImageView btn_shuffle;
    LinearLayout llShuffle;
    private boolean isShuffleActive = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.tmr_ringtone_fragment, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = getActivity();
        progress = new ProgressDialog(getActivity());
        sharedPref = mContext.getSharedPreferences("ShufflePrefs", mContext.MODE_PRIVATE);
        init();

        initListeners();
        initPlayBackManagerForFragment();

    }

    public void showProgressDialog() {
        progress.setTitle("Loading");
        progress.setMessage("Wait while loading...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();
    }


    private void initPlayBackManagerForFragment() {
        showProgressDialog();
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            playBackManagerForFragment = PlayBackManagerForFragment.getInstance(mContext, type);
        } else {
            if (mContext.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
                requestPermissions(new String[]{
                        Manifest.permission.READ_EXTERNAL_STORAGE
                }, 0);
            else
                playBackManagerForFragment = PlayBackManagerForFragment.getInstance(mContext, type);
        }
    }


    private void init() {
        lv_songslist = (ListView) getActivity().findViewById(R.id.recycler_allSongs);
        mLayout = (SlidingUpPanelLayout) getActivity().findViewById(R.id.sliding_layout);
        songAlbumbg = (ImageView) getActivity().findViewById(R.id.image_songAlbumbg_mid);
        ivListBG = (ImageView) getActivity().findViewById(R.id.iv_lvBG);
        img_bottom_slideone = (ImageView) getActivity().findViewById(R.id.img_bottom_slideone);
        img_bottom_slidetwo = (ImageView) getActivity().findViewById(R.id.img_bottom_slidetwo);

        llBottomLayout = (LinearLayout) getActivity().findViewById(R.id.ll_bottom);
        txt_timeprogress = (TextView) getActivity().findViewById(R.id.slidepanel_time_progress);
        txt_timetotal = (TextView) getActivity().findViewById(R.id.slidepanel_time_total);
        imgbtn_backward = (ImageView) getActivity().findViewById(R.id.btn_backward);
        imgbtn_forward = (ImageView) getActivity().findViewById(R.id.btn_forward);

        btn_playpause = (PlayPauseView) getActivity().findViewById(R.id.btn_play);
        progressBar = (ProgressBar) getActivity().findViewById(R.id.progressBar);

        btn_playpausePanel = (PlayPauseView) getActivity().findViewById(R.id.bottombar_play);
        llProgress = (LinearLayout) getActivity().findViewById(R.id.llProgress);

        btn_shuffle = (ImageView) getActivity().findViewById(R.id.btn_shuffle);
        llShuffle = (LinearLayout) getActivity().findViewById(R.id.llShuffle);

        if (sharedPref != null) {
            isShuffleActive = sharedPref.getBoolean("isShuffleActive", false);
            if (!isShuffleActive) {
                llShuffle.setBackgroundColor(getActivity().getResources().getColor(R.color.colorTransparent));
                btn_shuffle.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_shuffle_white_36dp));
            } else {
                llShuffle.setBackgroundColor(getActivity().getResources().getColor(R.color.white));
                btn_shuffle.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_shuffle_black_36dp));
            }
        }

        seekBar = (SeekBar) getActivity().findViewById(R.id.seekbar);
        seekBar.setEnabled(false);
        btn_playpausePanel.Pause();
        btn_playpause.Pause();

        TypedValue typedvaluecoloraccent = new TypedValue();
        getActivity().getTheme().resolveAttribute(R.attr.colorAccent, typedvaluecoloraccent, true);

        txt_playesongname = (TextView) getActivity().findViewById(R.id.txt_playesongname);
        txt_songartistname = (TextView) getActivity().findViewById(R.id.txt_songartistname);
        txt_playesongname_slidetoptwo = (TextView) getActivity().findViewById(R.id.txt_playesongname_slidetoptwo);
        txt_songartistname_slidetoptwo = (TextView) getActivity().findViewById(R.id.txt_songartistname_slidetoptwo);

        slidepanelchildtwo_topviewone = (RelativeLayout) getActivity().findViewById(R.id.slidepanelchildtwo_topviewone);
        slidepanelchildtwo_topviewtwo = (RelativeLayout) getActivity().findViewById(R.id.slidepanelchildtwo_topviewtwo);

        slidepanelchildtwo_topviewone.setVisibility(View.VISIBLE);
        slidepanelchildtwo_topviewtwo.setVisibility(View.INVISIBLE);
    }

    private void initListeners() {
        btn_playpausePanel.setOnClickListener(this);
        btn_playpause.setOnClickListener(this);
        imgbtn_backward.setOnClickListener(this);
        imgbtn_forward.setOnClickListener(this);

        slidepanelchildtwo_topviewone.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
            }
        });

        slidepanelchildtwo_topviewtwo.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            }
        });

        mLayout.setPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
                if (slideOffset == 0.0f) {
                    isExpand = false;
                    slidepanelchildtwo_topviewone.setVisibility(View.VISIBLE);
                    slidepanelchildtwo_topviewtwo.setVisibility(View.INVISIBLE);
                } else if (slideOffset > 0.0f && slideOffset < 1.0f) {
                    if (isExpand) {
//                        slidepanelchildtwo_topviewone.setAlpha(1.0f);
//                        slidepanelchildtwo_topviewtwo.setAlpha(1.0f -
//                                slideOffset);
                        slidepanelchildtwo_topviewone.setVisibility(View.VISIBLE);
                        slidepanelchildtwo_topviewtwo.setVisibility(View.INVISIBLE);
                    } else {
//                        slidepanelchildtwo_topviewone.setAlpha(1.0f -
//                                slideOffset);
//                        slidepanelchildtwo_topviewtwo.setAlpha(1.0f);
                        slidepanelchildtwo_topviewone.setVisibility(View.INVISIBLE);
                        slidepanelchildtwo_topviewtwo.setVisibility(View.VISIBLE);
                    }
                } else {
                    isExpand = true;
                    slidepanelchildtwo_topviewone.setVisibility(View.INVISIBLE);
                    slidepanelchildtwo_topviewtwo.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onPanelExpanded(View panel) {
                isExpand = true;
            }

            @Override
            public void onPanelCollapsed(View panel) {
                isExpand = false;
            }

            @Override
            public void onPanelAnchored(View panel) {
            }

            @Override
            public void onPanelHidden(View panel) {

            }
        });


        lv_songslist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                HashMap<String, String> hashMap = PlayBackManagerForFragment.songsList.get(position);
                loadSongInfo(hashMap, true);
                PlayBackManagerForFragment.playSong(hashMap);
                PlayBackManagerForFragment.setPlayingSongPref(hashMap);
                btn_playpause.Play();
                btn_playpausePanel.Play();

                llProgress.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.VISIBLE);
                btn_playpause.setVisibility(View.GONE);
                btn_playpausePanel.setVisibility(View.GONE);
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    shouldContinue = false;
                    PlayBackManagerForFragment.playPauseEvent(false, false, false, progress);
                }
                //SongService.player.seekTo(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                PlayBackManagerForFragment.showNotif(false);
            }
        });

        btn_shuffle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isShuffleActive) {
                    isShuffleActive = false;
                    llShuffle.setBackgroundColor(getActivity().getResources().getColor(R.color.colorTransparent));
                    btn_shuffle.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_shuffle_white_36dp));
                } else {
                    llShuffle.setBackgroundColor(getActivity().getResources().getColor(R.color.white));
                    btn_shuffle.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_shuffle_black_36dp));
                    isShuffleActive = true;
                }
                SharedPreferences.Editor prefEditor = sharedPref.edit();
                prefEditor.putBoolean("isShuffleActive", isShuffleActive);
                prefEditor.commit();
            }
        });
    }

    private Handler seekHandler = new Handler();
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            int currSeekPos = SongService.getCurrPos();
            int max = seekBar.getMax();
            if (currSeekPos > max) {
                currSeekPos = 0;
            }
            if (shouldContinue) {
                txt_timeprogress.setText(calculateDuration(currSeekPos));
                seekBar.setProgress(currSeekPos);
                seekHandler.postDelayed(runnable, 1000);
            }
            seekBar.setProgress(currSeekPos);
//            while (currSeekPos < max && shouldContinue) {
//                try {
//                    Thread.sleep(1000);
//                    currSeekPos = SongService.getCurrPos();
//                    final int finalCurrSeekPos = currSeekPos;
//                    if(finalCurrSeekPos<max){
//                        seekBar.setProgress(currSeekPos);
//                        getActivity().runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                txt_timeprogress.setText(calculateDuration(finalCurrSeekPos));
//                            }
//                        });
//                    }
//                } catch (InterruptedException e) {
//                    return;
//                } catch (Exception e) {
//                    return;
//                }
//            }
        }
    };

    public void setSeekProgress() {
        try {
            seekHandler.removeCallbacks(runnable);
            shouldContinue = true;
//        if (duration != seekBar.getMax())
//            seekBar.setMax(duration);
            if (!btn_playpause.isPlay()) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setPlayPauseView(true);
                    }
                });
            }
            getActivity().runOnUiThread(runnable);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setAllSongs(ArrayList<HashMap<String, String>> list) {
        mAllSongsListAdapter = new TmrBhajanAdapter(mContext, PlayBackManagerForFragment.songsList);
        lv_songslist.setAdapter(mAllSongsListAdapter);
        progress.dismiss();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.bottombar_play:

                if (PlayBackManagerForFragment.playPauseEvent(false, SongService.isPlaying(), true, seekBar.getProgress())) {
                    llProgress.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.VISIBLE);
                    btn_playpause.setVisibility(View.GONE);
                    btn_playpausePanel.setVisibility(View.GONE);

                    btn_playpause.Play();
                    btn_playpausePanel.Play();
                    thread = new Thread(runnable);
                    thread.start();
                } else {

                    PlayBackManagerForFragment.isManuallyPaused = true;
                    btn_playpause.Pause();
                    btn_playpausePanel.Pause();
                    shouldContinue = false;
                }
                break;

            case R.id.btn_play:

                if (PlayBackManagerForFragment.playPauseEvent(false, SongService.isPlaying(), true, seekBar.getProgress())) {
                    llProgress.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.VISIBLE);
                    btn_playpause.setVisibility(View.GONE);
                    btn_playpausePanel.setVisibility(View.GONE);

                    btn_playpause.Play();
                    btn_playpausePanel.Play();
                } else {
                    PlayBackManagerForFragment.isManuallyPaused = true;
                    btn_playpause.Pause();
                    btn_playpausePanel.Pause();
                    shouldContinue = false;
                }
                break;

            case R.id.btn_forward:
                llProgress.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.VISIBLE);
                btn_playpause.setVisibility(View.GONE);
                btn_playpausePanel.setVisibility(View.GONE);

                PlayBackManagerForFragment.playNext(isShuffleActive);
                break;

            case R.id.btn_backward:
                llProgress.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.VISIBLE);
                btn_playpause.setVisibility(View.GONE);
                btn_playpausePanel.setVisibility(View.GONE);

                PlayBackManagerForFragment.playPrev(isShuffleActive);
                break;

            default:
                break;
        }

    }

    public void loadSongInfo(HashMap<String, String> songDetail, boolean seeking) {
        String title = songDetail.get(SONG_TITLE);
        String artist = songDetail.get(ARTIST_NAME);
        String path = songDetail.get(SONG_PATH);
        String dur = songDetail.get(SONG_DURATION);
        float milliSecDuration = Float.parseFloat(songDetail.get(SONG_DURATION));
        String progress = songDetail.get(SONG_PROGRESS);
        int milliSecProgress = Integer.parseInt(progress == null ? "0" : progress);
        if (txt_playesongname != null) {
            txt_playesongname.setText(title);
            txt_playesongname_slidetoptwo.setText(title);
            txt_songartistname.setText(artist);
            txt_songartistname_slidetoptwo.setText(artist);
            txt_timetotal.setText(calculateDuration(milliSecDuration));
            seekBar.setMax((int) milliSecDuration);
            txt_timeprogress.setText(calculateDuration(milliSecProgress));
            seekBar.setEnabled(true);
            if (seeking) {
                setSeekProgress();
            } else {
                seekBar.setProgress(milliSecProgress);
            }
        }

//        try {
//            BitmapPalette.getColorsFromBitmap(mContext, path, false);
//        } catch (RuntimeException e) {
//            e.printStackTrace();
//        }
//        updateProgress(songsManager);
    }

    public void setBitmapColors() {
        if (songAlbumbg != null) {
            if (mediumBitmap != null) {
                ivListBG.setImageBitmap(blurredBitmap);
                songAlbumbg.setImageBitmap(mediumBitmap);
                img_bottom_slideone.setImageBitmap(smallBitmap);
                img_bottom_slidetwo.setImageBitmap(smallBitmap);
                toolbar.setBackgroundColor(BitmapPalette.darkVibrantRGBColor);
                toolbar.setTitleTextColor(BitmapPalette.darkVibrantTitleTextColor);
                slidepanelchildtwo_topviewone.setBackgroundColor(BitmapPalette.darkVibrantRGBColor);
                slidepanelchildtwo_topviewtwo.setBackgroundColor(BitmapPalette.darkVibrantRGBColor);
                txt_playesongname.setTextColor(BitmapPalette.darkVibrantBodyTextColor);
                txt_playesongname_slidetoptwo.setTextColor(BitmapPalette.darkVibrantBodyTextColor);
                txt_songartistname.setTextColor(BitmapPalette.darkVibrantBodyTextColor);
                txt_songartistname_slidetoptwo.setTextColor(BitmapPalette.darkVibrantBodyTextColor);
                txt_timetotal.setTextColor(BitmapPalette.darkVibrantBodyTextColor);
                txt_timeprogress.setTextColor(BitmapPalette.darkVibrantBodyTextColor);
                llBottomLayout.setBackgroundColor(BitmapPalette.darkVibrantRGBColor);
            } else {
                songAlbumbg.setImageResource(R.drawable.bg2);
                img_bottom_slideone.setImageResource(R.drawable.bg2);
                img_bottom_slidetwo.setImageResource(R.drawable.ic_keyboard_backspace_white_18dp);
            }
        }
    }


    public String calculateDuration(float millisec) {
        float sec = millisec * 1000 * 60 / 1000;
        return sec != 0 ? String.format("%f:%02f", sec / 60, sec % 60) : "0:00";
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 0:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    playBackManagerForFragment = PlayBackManagerForFragment.getInstance(mContext, type);
                break;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (playBackManagerForFragment != null) {
            HashMap hashMap = PlayBackManagerForFragment.getPlayingSongPref();
            Object value = hashMap.get("songTitle");
            if (!value.equals("")) {
                loadSongInfo(PlayBackManagerForFragment.getPlayingSongPref(), SongService.isPlaying());
                setPlayPauseView(SongService.isPlaying());
            }
        } else initPlayBackManagerForFragment();

        getActivity().registerReceiver(broadcastReceiver, new IntentFilter(SongService.BROADCAST_ACTION));
    }

//    @Override
//    public void onBackPressed() {
//        if (mLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED) {
//            mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
//        } else if (SongService.isPlaying()) {
//            super.onBackPressed();
//        } else {
//            super.onBackPressed();
//            finish();
//        }
//    }

    @Override
    public void onDestroy() {
        if (PlayBackManagerForFragment.isServiceRunning && !SongService.isPlaying())
            PlayBackManagerForFragment.stopService();

        getActivity().unregisterReceiver(broadcastReceiver);
        super.onDestroy();
    }

    public void setPlayPauseView(boolean isPlaying) {

        if (btn_playpause != null)
            if (isPlaying) {
                btn_playpause.Play();
                btn_playpausePanel.Play();
            } else {
                btn_playpause.Pause();
                btn_playpausePanel.Pause();
                shouldContinue = false;
            }
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            loadSongInfo(PlayBackManagerForFragment.getPlayingSongPref(), SongService.isPlaying());
            String isProgress = intent.getStringExtra("isProgress");

            if (isProgress.equalsIgnoreCase("true")) {
                llProgress.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.VISIBLE);
                btn_playpause.setVisibility(View.GONE);
                btn_playpausePanel.setVisibility(View.GONE);
            } else {
                llProgress.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                btn_playpause.setVisibility(View.VISIBLE);
                btn_playpausePanel.setVisibility(View.VISIBLE);
            }

        }
    };
}