package com.kumar.raman.shrikrishan.Adapter;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.kumar.raman.shrikrishan.Activity.WallpaperAdapter;
import com.kumar.raman.shrikrishan.PhotoFullPopupWindow;
import com.kumar.raman.shrikrishan.Pojo.ImagesData;
import com.kumar.raman.shrikrishan.R;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.List;

import static android.content.ContentValues.TAG;

/**
 * Created by mann on 20/2/18.
 */

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.MyViewHolder>{
    FragmentTransaction ft;
    private List<ImagesData> imageList;
    ProgressDialog progressDialog;
    ShareFacebook shareFacebook;
    ProgressDialog newProgressDialog;

    Context mContext;
    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textview1;
        ImageView imageView;
        private ItemClickListener clickListener;

        public MyViewHolder(View view) {
            super(view);

            textview1 = (TextView) view.findViewById(R.id.title);
            imageView=(ImageView) view.findViewById(R.id.wallpaper_image);
            imageView.setOnClickListener(this);
        }

        public void setClickListener(GalleryAdapter.ItemClickListener itemClickListener) {
            this.clickListener = itemClickListener;
        }

        @Override
        public void onClick(View view) {
            clickListener.onClick(view, getPosition(), false);
        }
    }


    public GalleryAdapter(List<ImagesData> arrayList, Context context, FragmentTransaction ft) {
        this.imageList = arrayList;
        this.mContext = context;
        this.ft=ft;
    }

    @Override
    public GalleryAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.gallery_adapter, parent, false);
        return new GalleryAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
       final String images=imageList.get(position).getFullImage();
        final String title= imageList.get(position).getThumbnal();
        Picasso.with(mContext)
                .load(images)
                .placeholder(R.drawable.placeholder_image)
                .into(holder.imageView);
        holder.textview1.setText(Html.fromHtml(title));

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new PhotoFullPopupWindow(mContext, R.layout.popup_photo_full, view, images, null);

            }
        });
        holder.imageView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
               // downloadImage(images,title);
                showDialog(images,title);
                return true;
            }
        });

    }
    public void setImageListner(GalleryAdapter.ShareFacebook shareFacebook) {
        this.shareFacebook= shareFacebook;
    }
    public void showDialog(final String images,final String title){
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_downloadshare);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        final ImageView download=(ImageView)dialog.findViewById(R.id.download);
        final ImageView shareButton = (ImageView)dialog.findViewById(R.id.fb_share_button);
        //dialog.setCancelable(true);

        download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if(isStoragePermissionGranted()){
                    //downloadImage(images,title);
                dialog.dismiss();
                shareFacebook.downloadImageFromURL(images,title);
                }
            }
        });
        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                newProgressDialog=shareFacebook.showProgress();
                shareFacebook.shareImage(images,title,newProgressDialog);
                dialog.dismiss();
            }
        });
        dialog.show();
    }
    @Override
    public int getItemCount() {
        return imageList.size();
    }
    public interface ItemClickListener {
        void onClick(View view, int position, boolean isLongClick);
    }
    public void showProgressDialog(){
        progressDialog.setTitle("Loading");
        progressDialog.setMessage("Image downloading");
        progressDialog.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progressDialog.show();
    }
    public void downloadImage(String images,final String title){
    Picasso.with(mContext)
            .load(images)
            .into(new Target() {
                      @Override
                      public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                          try {
                              String root = Environment.getExternalStorageDirectory().toString();
                              File myDir = new File(root + "/srikrishna/images");

                              if (!myDir.exists()) {
                                  myDir.mkdirs();
                              }

                              String name = new Date().toString() + ".jpg";
                              myDir = new File(myDir, name);
                              FileOutputStream out = new FileOutputStream(myDir);
                              bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
//progressDialog.dismiss();
                              out.flush();
                              out.close();
                              MediaStore.Images.Media.insertImage(mContext.getContentResolver(), bitmap, title , title);
                              try {
                                  new Handler().postDelayed(new Runnable() {
                                      @Override
                                      public void run() {
                                          Toast.makeText(mContext, " Download completed", Toast.LENGTH_SHORT).show();
                                      }
                                  }, 4000);
                              }catch (Exception e){
                                  e.printStackTrace();

                              }
                              //Toast.makeText(mContext, " Download completed", Toast.LENGTH_SHORT).show();

                          } catch(Exception e){
                              e.printStackTrace();
                              // some action
                          }
                      }

                      @Override
                      public void onBitmapFailed(Drawable errorDrawable) {
                          Toast.makeText(mContext, " Download failed", Toast.LENGTH_SHORT).show();

                          //progressDialog.dismiss();
                      }

                      @Override
                      public void onPrepareLoad(Drawable placeHolderDrawable) {
                          Toast.makeText(mContext, " Download failes", Toast.LENGTH_SHORT).show();

                      }
                  }
            );
    }
    public  boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (mContext.checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG,"Permission is granted");
                return true;
            } else {
                Log.v(TAG,"Permission is revoked");
                ActivityCompat.requestPermissions((Activity) mContext, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG,"Permission is granted");
            return true;
        }
    }
    public interface ShareFacebook {
        public void shareImage(String image,String title,ProgressDialog dialog);
        public ProgressDialog showProgress();
        public void dismissProgressDialog(ProgressDialog progressDialog);
        public ProgressDialog showDialoadingProgress();
        public void downloadImageFromURL(String image,String title);
    }

}