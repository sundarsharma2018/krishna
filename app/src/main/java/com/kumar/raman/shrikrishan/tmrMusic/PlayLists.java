package com.kumar.raman.shrikrishan.tmrMusic;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kumar.raman.shrikrishan.R;

/**
 * Created by rahul on 6/22/2017.
 */

public class PlayLists extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.playlists, container, false);
        return v;
    }
}
