package com.kumar.raman.shrikrishan.Activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.android.volley.NetworkResponse;
import com.android.volley.VolleyError;
import com.kumar.raman.shrikrishan.Adapter.VideoAdapter;
import com.kumar.raman.shrikrishan.Pojo.AudioModel;
import com.kumar.raman.shrikrishan.R;
import com.kumar.raman.shrikrishan.networking.NetworkingCallbackInterface;
import com.kumar.raman.shrikrishan.networking.RequestHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dell- on 8/4/2018.
 */

public class VideoActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    ProgressDialog progress;
    private VideoAdapter mAdapter;
    private List<AudioModel> videoList = new ArrayList<>();
    String url="http://ramankumarynr.com/api/get_posts/?post_type=youtube_api&count=50";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.video_activity_lay);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        setTitle("Videos");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        progress = new ProgressDialog(this);

       getAllVideoUrl();
    }
    public void getAllVideoUrl() {

        JSONObject json =new JSONObject();
        showProgressDialog();

        RequestHandler.getVideo(url,this,json, new NetworkingCallbackInterface() {
            @Override
            public void onSuccess(NetworkResponse response, boolean fromCache) {
                System.out.print("response........"+response);
                try {
                    JSONArray jsonArray=new JSONArray(response);
                    System.out.print("jsonArrayresponse........"+jsonArray);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onSuccess(String response, boolean fromCache) {
                System.out.print("response........"+response);
                progress.dismiss();
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    JSONArray jsonArray=jsonObject.getJSONArray("posts");
                    AudioModel content=new AudioModel();

                    for(int k=0;k<jsonArray.length();k++){
                        JSONObject postsJson= jsonArray.getJSONObject(k);
                       JSONObject fieldJSON= postsJson.getJSONObject("custom_fields");
                        JSONArray jsonArray1=fieldJSON.getJSONArray("youtube_heading");
                        JSONArray youtube_embedded_code=fieldJSON.getJSONArray("youtube_embedded_code");
                        content=new AudioModel();
                        String title= jsonArray1.get(0).toString();
                        String url= youtube_embedded_code.get(0).toString();
                        content.setTitle(title);
                        content.setUrl(url);

                        videoList.add(content);
                    }

                    mAdapter = new VideoAdapter(videoList,VideoActivity.this,getSupportFragmentManager()
                            .beginTransaction());
                    recyclerView.setAdapter(mAdapter);
                    //System.out.print("jsonObjectArrayresponse........"+json);
                    progress.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(VolleyError error) {
            }

            @Override
            public void onNetworkFailure(String error) {

            }
        });
    }
    public void showProgressDialog(){
        progress.setTitle("Loading");
        progress.setMessage("Wait while loading...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();
    }
}
