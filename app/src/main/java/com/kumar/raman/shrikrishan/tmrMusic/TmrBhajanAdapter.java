package com.kumar.raman.shrikrishan.tmrMusic;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.kumar.raman.shrikrishan.NewAudioActivity;
import com.kumar.raman.shrikrishan.Pojo.AudioModel;
import com.kumar.raman.shrikrishan.R;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.content.ContentValues.TAG;

/**
 * Created by Sachin Rathore on 7/27/2019.
 */

public class TmrBhajanAdapter extends BaseAdapter {

    private LayoutInflater layoutInflater;
    private ArrayList<HashMap<String, String>> sList;
    Context mContext;

    public TmrBhajanAdapter(Context mContext, ArrayList<HashMap<String, String>> sList) {
        this.mContext = mContext;
        this.layoutInflater = LayoutInflater.from(mContext);
        this.sList = sList;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder mViewHolder;
        if (convertView == null) {
            mViewHolder = new ViewHolder();
            convertView = layoutInflater.inflate(R.layout.audio_list_row, null);

            mViewHolder.textview1 = (LinearLayout) convertView.findViewById(R.id.textview1);
            mViewHolder.main_layout = (RelativeLayout) convertView.findViewById(R.id.main_layout);
            mViewHolder.stop = (LinearLayout) convertView.findViewById(R.id.stop);
            mViewHolder.alarm_icon = (LinearLayout) convertView.findViewById(R.id.alarm_icon);
            mViewHolder.play_icon = (LinearLayout) convertView.findViewById(R.id.play_icon);
            mViewHolder.text = (TextView) convertView.findViewById(R.id.text);
            mViewHolder.tvDuration = (TextView) convertView.findViewById(R.id.tvDuration);
            mViewHolder.llDuration = (LinearLayout) convertView.findViewById(R.id.llDuration);

            mViewHolder.stop.setVisibility(View.GONE);
            mViewHolder.play_icon.setVisibility(View.GONE);
            mViewHolder.alarm_icon.setVisibility(View.GONE);
            mViewHolder.llDuration.setVisibility(View.VISIBLE);

            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (ViewHolder) convertView.getTag();
        }
        String title = "";
        String path = "";
        if (position < sList.size()) {
            title = sList.get(position).get("songTitle");
            path = sList.get(position).get("songPath");
        }
        String dur = sList.get(position).get("songDuration").replace("f","");
        mViewHolder.tvDuration.setText(dur.replace(".",":"));

        mViewHolder.text.setText(title);
//        mViewHolder.textViewSongArtisNameAndDuration.setText(((TmrMusicNewActivity) mContext)
//                .calculateDuration(Integer.parseInt(sList.get(position).get("songDuration")))
//                + " | " + sList.get(position).get("artistName"));
//        Bitmap bitmap = BitmapPalette.getBitmapFromMediaPath(mContext, path, true);


        return convertView;
    }

    @Override
    public int getCount() {
        return (sList != null) ? sList.size() : 0;
    }

    class ViewHolder {
        LinearLayout textview1;
        LinearLayout stop, alarm_icon;
        RelativeLayout main_layout;
        LinearLayout play_icon;
        TextView text;
        LinearLayout llDuration;
        TextView tvDuration;
    }

}
