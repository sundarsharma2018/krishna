package com.kumar.raman.shrikrishan;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.widget.TextView;

public class FullScreenTextActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen_text);
         setTitle("Arati");
        TextView txtview_content=(TextView) findViewById(R.id.content);

        TextView title=(TextView) findViewById(R.id.title);


        String content=getIntent().getStringExtra("content");
        String titletext=getIntent().getStringExtra("title");
        txtview_content.setText(Html.fromHtml(content));
        title.setText(titletext);
    }

}
