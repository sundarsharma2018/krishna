package com.kumar.raman.shrikrishan.Pojo;

/**
 * Created by Dell- on 3/10/2018.
 */

public class ImagesData {

    String thumbnal;
    String fullImage;

    public String getThumbnal() {
        return thumbnal;
    }

    public void setThumbnal(String thumbnal) {
        this.thumbnal = thumbnal;
    }

    public String getFullImage() {
        return fullImage;
    }

    public void setFullImage(String fullImage) {
        this.fullImage = fullImage;
    }
}
