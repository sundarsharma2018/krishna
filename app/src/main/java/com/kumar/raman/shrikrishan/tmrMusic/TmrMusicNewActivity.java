package com.kumar.raman.shrikrishan.tmrMusic;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.kumar.raman.shrikrishan.AudioFragment;
import com.kumar.raman.shrikrishan.R;
import com.kumar.raman.shrikrishan.tmrMusic.fragments.TmrBhajanFragment;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Sachin Rathore on 7/27/2019.
 */

public class TmrMusicNewActivity extends AppCompatActivity {

    public static final String SONG_TITLE = "songTitle";
    public static final String DISPLAY_NAME = "displayName";
    public static final String SONG_ID = "songID";
    public static final String SONG_PATH = "songPath";
    public static final String ALBUM_NAME = "albumName";
    public static final String ARTIST_NAME = "artistName";
    public static final String SONG_DURATION = "songDuration";
    public static final String SONG_POS = "songPosInList";
    public static final String SONG_PROGRESS = "songProgress";

    public static TmrMusicNewActivity instance;
    public static boolean shouldContinue;
    TabLayout tabLayout;
    ViewPager viewPager;
    TabAdapter tabAdapter;

    TmrBhajanFragment tmrBhajanFragment;
    AudioFragment audioFragment;
    int fragIndex = 0;
    private boolean bound = false;

    public static TmrMusicNewActivity getInstance() {
        return instance;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.tmr_new_music_activity);
        instance = this;


        tabLayout = findViewById(R.id.tabLayout);
        viewPager = findViewById(R.id.viewPager);

        initViewPager();
    }



    private void initViewPager() {
        tabLayout.addTab(tabLayout.newTab().setText("RingTone/Alarm"));
        tabLayout.addTab(tabLayout.newTab().setText("Bhajan"));

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                fragIndex = tab.getPosition();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        setAdapter();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode==1){
            audioFragment.setPermission();
        }
    }

    private void setAdapter() {
        tabAdapter = new TabAdapter(getSupportFragmentManager());
        audioFragment = new AudioFragment();
        tmrBhajanFragment = new TmrBhajanFragment();

        tabAdapter.addFragment(audioFragment);
        tabAdapter.addFragment(tmrBhajanFragment);

        viewPager.setAdapter(tabAdapter);
    }


    public void setAllSongs(String type, ArrayList<HashMap<String, String>> list) {
        if (type.equalsIgnoreCase("ringtone"))
            getInstance().tmrBhajanFragment.setAllSongs(list);
    }

    public void setPlayPauseView(String type, boolean b) {
        getInstance().tmrBhajanFragment.setPlayPauseView(b);
    }

    public void loadSongInfo(String type, HashMap<String, String> hashMap, boolean b) {
        getInstance().tmrBhajanFragment.loadSongInfo(hashMap, b);
    }

    public void setSeekProgress(String type) {
        getInstance().tmrBhajanFragment.setSeekProgress();
    }

    public String calculateDuration(String type, int songDuration) {
        getInstance().tmrBhajanFragment.calculateDuration(songDuration);
        return "";
    }
}
