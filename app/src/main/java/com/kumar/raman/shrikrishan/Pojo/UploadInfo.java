package com.kumar.raman.shrikrishan.Pojo;

/**
 * Created by Dell- on 3/21/2018.
 */

public class UploadInfo {

    public String name;
    public String url;

    public UploadInfo() {
    }

    public UploadInfo(String name, String url) {
        this.name = name;
        this.url= url;
    }
}
