package com.kumar.raman.shrikrishan.tmrMusic;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.util.Log;

import com.kumar.raman.shrikrishan.tmrMusic.helpers.BitmapPalette;
import com.kumar.raman.shrikrishan.tmrMusic.helpers.Listeners;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Sachin Rathore on 7/27/2019.
 */

public class PlayBackManagerForFragment {
    public static boolean isFirstLoad = true;
    private Uri allsongsuri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
    private String[] projectionSongs = {MediaStore.Audio.Media._ID, MediaStore.Audio.Media.ARTIST,
            MediaStore.Audio.Media.TITLE, MediaStore.Audio.Media.DATA, MediaStore.Audio.Media.ALBUM,
            MediaStore.Audio.Media.DISPLAY_NAME, MediaStore.Audio.Media.DURATION};
    private String selection = MediaStore.Audio.Media.IS_MUSIC + " != 0";
    public static ArrayList<HashMap<String, String>> songsList;
    public static ArrayList<Integer> shufflePosList;
    public static final String songPref = "songPref";
    private static Context mContext;
    private static SharedPreferences sharedPref;
    public static boolean isServiceRunning = false, isManuallyPaused = false;
    private static PlayBackManagerForFragment playbackManager;
    static String fragType;
    boolean isExecuted;

    private PlayBackManagerForFragment(Context mContext, String type) {
        this.mContext = mContext;
        isFirstLoad = true;
        sharedPref = mContext.getSharedPreferences(songPref, mContext.MODE_PRIVATE);
        fragType = type;
        createPlayList(type);
    }

//    private PlayBackManagerForFragment(Context mContext, Listeners.LoadSongListener loadSongListener, Uri allsongsuri) {
//        this.mContext = mContext;
//        sharedPref = mContext.getSharedPreferences(songPref, mContext.MODE_PRIVATE);
//        if (loadSongListener != null)
//            this.loadSongListener = loadSongListener;
//        if (allsongsuri != null)
//            this.allsongsuri = allsongsuri;
//        createPlayList("");
//    }

    public static PlayBackManagerForFragment getInstance(Context mContext, String type) {
        playbackManager = null;
        songsList = new ArrayList<>();
        shufflePosList = new ArrayList<>();
        goAhead = true;
        playbackManager = new PlayBackManagerForFragment(mContext, type);
        fragType = type;
        return playbackManager;
    }

    private Listeners.LoadSongListener loadSongListener = new Listeners.LoadSongListener() {

        @Override
        public void onSongLoaded(ArrayList<HashMap<String, String>> list) {
            ((TmrMusicNewActivity) mContext).setAllSongs(fragType, list);
        }
    };

    private void createPlayList(final String fragType) {
        new PlayBackManagerForFragment.LoadSongsAsync(fragType).execute();
    }

    private class LoadSongsAsync extends AsyncTask<String, Void, String> {
        String musicType;

        public LoadSongsAsync(String musicType) {
            this.musicType = musicType;
        }

        static final String REQUEST_METHOD = "GET";
        static final int READ_TIMEOUT = 15000;
        static final int CONNECTION_TIMEOUT = 15000;

        @Override
        protected String doInBackground(String... params) {

            String result;
            String inputLine;
            String m_URL = "https://ramankumarynr.com/api/?json=get_post&id=1294";

            try {
                //Create a URL object holding our url
                URL myUrl = new URL(m_URL);
                //Create a connection
                HttpURLConnection connection = (HttpURLConnection)
                        myUrl.openConnection();
                //Set methods and timeouts
                connection.setRequestMethod(REQUEST_METHOD);
                connection.setReadTimeout(READ_TIMEOUT);
                connection.setConnectTimeout(CONNECTION_TIMEOUT);

                //Connect to our url
                connection.connect();
                //Create a new InputStreamReader
                InputStreamReader streamReader = new
                        InputStreamReader(connection.getInputStream());
                //Create a new buffered reader and String Builder
                BufferedReader reader = new BufferedReader(streamReader);
                StringBuilder stringBuilder = new StringBuilder();
                //Check if the line we are reading is not null
                while ((inputLine = reader.readLine()) != null) {
                    stringBuilder.append(inputLine);
                }
                //Close our InputStream and Buffered reader
                reader.close();
                streamReader.close();
                //Set our result equal to our stringBuilder
                result = stringBuilder.toString();
                Log.d("TAG", "" + result);
            } catch (IOException e) {
                e.printStackTrace();
                result = null;
            }
            return result;
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONObject json = jsonObject.getJSONObject("post");
                JSONArray arrayAttachment = json.getJSONArray("attachments");
                for (int j = 0; j < arrayAttachment.length(); j++) {
                    JSONObject attachJson = arrayAttachment.getJSONObject(j);


                    HashMap<String, String> song = new HashMap<String, String>();
                    String song_title = attachJson.getString("title");
                    String display_name = attachJson.getString("title");
                    int song_id = attachJson.getInt("id");

                    String song_path = attachJson.getString("url");

                    String album_name = attachJson.getString("title");

                    String artist_name = attachJson.getString("slug");

                    String song_duration = attachJson.getString("caption").equals("") ? "00.00f" : attachJson.getString("caption") + "f";
                    song_duration = song_duration.replace(":", ".");
                    song.put(TmrMusicNewActivity.SONG_TITLE, song_title);
                    song.put(TmrMusicNewActivity.DISPLAY_NAME, display_name);
                    song.put(TmrMusicNewActivity.SONG_ID, "" + song_id);
                    song.put(TmrMusicNewActivity.SONG_PATH, song_path);
                    song.put(TmrMusicNewActivity.ALBUM_NAME, album_name);
                    song.put(TmrMusicNewActivity.ARTIST_NAME, artist_name);
                    song.put(TmrMusicNewActivity.SONG_DURATION, song_duration);
                    song.put(TmrMusicNewActivity.SONG_POS, "" + j);
                    songsList.add(song);

                }

                HashMap hashMap = songsList.get(0);
                songsList.remove(0);
                songsList.add(hashMap);
                HashMap hashMap1 = getPlayingSongPref();
                Object val = hashMap1.get("songTitle");
                if (val.equals("")) {
                    setPlayingSongPref(hashMap);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }catch (Exception e){
                e.printStackTrace();
            }
            loadSongListener.onSongLoaded(songsList);
        }
    }

    public static void stopService() {
        mContext.startService(new Intent(mContext, SongService.class).setAction(SongService.ACTION_STOP));
    }

    public static void onStopService() {
        ((TmrMusicNewActivity) mContext).setPlayPauseView(fragType, false);
        shufflePosList.clear();
        songsList.clear();
        songsList = null;
        shufflePosList = null;
        sharedPref = null;

        playbackManager = null;
        mContext = null;
        isServiceRunning = false;
        isFirstLoad = true;
        isManuallyPaused = false;

        if (BitmapPalette.smallBitmap != null) {
            BitmapPalette.smallBitmap.recycle();
            BitmapPalette.mediumBitmap.recycle();
        }
    }

    public static boolean playPauseEvent(boolean headphone, boolean isPlaying, boolean isResume, int seekProgress) {
        HashMap<String, String> hashMap = getPlayingSongPref();
        if (headphone || isPlaying) {
            goAhead = false;
            ((TmrMusicNewActivity) mContext).setPlayPauseView(fragType, false);
            mContext.startService(
                    new Intent(mContext, SongService.class).setAction(SongService.ACTION_PAUSE));
            hashMap.put(TmrMusicNewActivity.SONG_PROGRESS, "" + SongService.getCurrPos());
            setPlayingSongPref(hashMap);
            return false;
        } else {
            isManuallyPaused = false;
            if (hashMap != null && hashMap.get(TmrMusicNewActivity.SONG_ID) != null) {
                if (seekProgress != -1)
                    seekTo(seekProgress, isResume, hashMap);
                else playSong(hashMap);
                return true;
            }
        }
        hashMap.clear();
        return false;
    }

    public static boolean goAhead = true;

    public static void playSong(final HashMap<String, String> hashMap) {
        goAhead = false;
        isManuallyPaused = false;
        setPlayingSongPref(hashMap);
        new Thread(new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(mContext, SongService.class);
                i.setAction(SongService.ACTION_PLAY);
                i.putExtra(TmrMusicNewActivity.SONG_PATH, hashMap.get(TmrMusicNewActivity.SONG_PATH));
                i.putExtra(TmrMusicNewActivity.SONG_TITLE, hashMap.get(TmrMusicNewActivity.SONG_TITLE));
                i.putExtra(TmrMusicNewActivity.ARTIST_NAME, hashMap.get(TmrMusicNewActivity.ARTIST_NAME));
                i.putExtra(TmrMusicNewActivity.ALBUM_NAME, hashMap.get(TmrMusicNewActivity.ALBUM_NAME));
                mContext.startService(i);
                int pos = Integer.parseInt(hashMap.get(TmrMusicNewActivity.SONG_POS));
                if (!shufflePosList.contains(pos)) {
                    shufflePosList.add(pos);
                }
            }
        }).start();
        //((TmrMusicNewActivity) mContext).loadSongInfo(hashMap, true);
    }

    private static void seekTo(final int progress, final boolean resume, final HashMap<String, String> hashMap) {
        if (goAhead) {
            goAhead = false;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Intent i = new Intent(mContext, SongService.class);
                    if (hashMap != null) {
                        i.putExtra(TmrMusicNewActivity.SONG_PATH, hashMap.get(TmrMusicNewActivity.SONG_PATH));
                        i.putExtra(TmrMusicNewActivity.SONG_TITLE, hashMap.get(TmrMusicNewActivity.SONG_TITLE));
                        i.putExtra(TmrMusicNewActivity.ARTIST_NAME, hashMap.get(TmrMusicNewActivity.ARTIST_NAME));
                        i.putExtra(TmrMusicNewActivity.ALBUM_NAME, hashMap.get(TmrMusicNewActivity.ALBUM_NAME));
                    }
                    i.setAction(SongService.ACTION_SEEK);
                    i.putExtra("seekTo", progress);
                    if (resume) i.putExtra("resume", true);
                    else i.putExtra("resume", false);
                    mContext.startService(i);
                    int pos = Integer.parseInt(hashMap.get(TmrMusicNewActivity.SONG_POS));
                    if (!shufflePosList.contains(pos)) {
                        shufflePosList.add(pos);
                    }
                }
            }).start();
            ((TmrMusicNewActivity) mContext).loadSongInfo(fragType, hashMap, true);
        }
    }

    public static void playNext(boolean isShuffle) {
        if (goAhead) {
            goAhead = false;
            TmrMusicNewActivity.shouldContinue = false;
            String lastPos = getPlayingSongPref().get(TmrMusicNewActivity.SONG_POS);
            int pos = Integer.parseInt((lastPos == null) ? "0" : lastPos);
            if (isShuffle) {
                if (shufflePosList != null)
                    if (shufflePosList.contains(pos)) {
                        int index = shufflePosList.indexOf(pos);
                        if (index < (shufflePosList.size() - 1))
                            pos = shufflePosList.get(++index);
                        else pos = shufflePos(false);
                    } else pos = shufflePos(false);
            } else pos += 1;
            if (songsList != null)
                if (pos > -1 && pos < songsList.size()) {
                    HashMap<String, String> hashMap = songsList.get(pos - 1);
                    playSong(hashMap);
                }
        }
    }

    public static void playPrev(boolean isShuffle) {
        if (goAhead) {
            goAhead = false;
            TmrMusicNewActivity.shouldContinue = false;
            int pos = Integer.parseInt(getPlayingSongPref().get(TmrMusicNewActivity.SONG_POS));
            if (isShuffle && shufflePosList.contains(pos)) {
                int index = shufflePosList.indexOf(pos);
                if (index != 0) pos = shufflePosList.get(--index);
                else {
                    pos = shufflePos(true);
                }
            } else pos -= 1;
            if (pos > -1 && pos < songsList.size()) {
                HashMap<String, String> hashMap = songsList.get(pos - 1);
                playSong(hashMap);
            }
        }
    }

    public static void mediaPlayerStarted(MediaPlayer mp) {
        ((TmrMusicNewActivity) mContext).setSeekProgress(fragType);
    }

    public static void showNotif(boolean updateColors) {
//        if (updateColors)
//            ((TmrMusicNewActivity) mContext).setBitmapColors(fragType);
        if (!isFirstLoad)
            mContext.startService(new Intent(mContext, SongService.class).setAction(SongService.UPDATE_NOTIF));
    }

    public static void setPlayingSongPref(final HashMap<String, String> songDetail) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                SharedPreferences.Editor prefEditor = sharedPref.edit();
                prefEditor.putString(TmrMusicNewActivity.SONG_TITLE, songDetail.get(TmrMusicNewActivity.SONG_TITLE))
                        .putString(TmrMusicNewActivity.SONG_ID, songDetail.get(TmrMusicNewActivity.SONG_ID))
                        .putString(TmrMusicNewActivity.ARTIST_NAME, songDetail.get(TmrMusicNewActivity.ARTIST_NAME))
                        .putString(TmrMusicNewActivity.ALBUM_NAME, songDetail.get(TmrMusicNewActivity.ALBUM_NAME))
                        .putString(TmrMusicNewActivity.SONG_DURATION, songDetail.get(TmrMusicNewActivity.SONG_DURATION))
                        .putString(TmrMusicNewActivity.SONG_PATH, songDetail.get(TmrMusicNewActivity.SONG_PATH))
                        .putString(TmrMusicNewActivity.SONG_POS, songDetail.get(TmrMusicNewActivity.SONG_POS))
                        .putString(TmrMusicNewActivity.SONG_PROGRESS, songDetail.get(TmrMusicNewActivity.SONG_PROGRESS));

                prefEditor.commit();
            }
        }).start();
    }

    public static HashMap<String, String> getPlayingSongPref() {
        HashMap<String, String> hashMap = new HashMap<>();
        if (sharedPref != null) {
            hashMap.put(TmrMusicNewActivity.SONG_TITLE, sharedPref.getString(TmrMusicNewActivity.SONG_TITLE, ""));
            hashMap.put(TmrMusicNewActivity.SONG_ID, sharedPref.getString(TmrMusicNewActivity.SONG_ID, ""));
            hashMap.put(TmrMusicNewActivity.ARTIST_NAME, sharedPref.getString(TmrMusicNewActivity.ARTIST_NAME, ""));
            hashMap.put(TmrMusicNewActivity.ALBUM_NAME, sharedPref.getString(TmrMusicNewActivity.ALBUM_NAME, ""));
            hashMap.put(TmrMusicNewActivity.SONG_DURATION, sharedPref.getString(TmrMusicNewActivity.SONG_DURATION, "" + 0));
            hashMap.put(TmrMusicNewActivity.SONG_PATH, sharedPref.getString(TmrMusicNewActivity.SONG_PATH, ""));
            hashMap.put(TmrMusicNewActivity.SONG_POS, sharedPref.getString(TmrMusicNewActivity.SONG_POS, -1 + ""));
            hashMap.put(TmrMusicNewActivity.SONG_PROGRESS, sharedPref.getString(TmrMusicNewActivity.SONG_PROGRESS, 0 + ""));
        }
        return hashMap;
    }

    private static int shufflePos(boolean isPrev) {
        int min = 0, max = (songsList.size() - 1);
        int range = (max - min) + 1;
        int shuffledPos = (int) (Math.random() * range) + min;
        if (!shufflePosList.contains(shuffledPos)) {
            if (!isPrev)
                shufflePosList.add(shuffledPos);
            else {
                int currPos = Integer.parseInt(getPlayingSongPref().get(TmrMusicNewActivity.SONG_POS));
                if (shufflePosList.contains(currPos)) {
                    shufflePosList.add((shufflePosList.indexOf(currPos)), shuffledPos);
                }
            }
            return shuffledPos;
        }
//        if (shuffledPos != Integer.parseInt(getPlayingSongPref().get(TmrMusicNewActivity.SONG_POS)))
//            return shuffledPos;
        else if (shufflePosList.size() < songsList.size())
            return shufflePos(isPrev);
        else {
            shufflePosList.clear();
            return shufflePos(isPrev);
        }
    }

}
