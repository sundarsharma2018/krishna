package com.kumar.raman.shrikrishan;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kumar.raman.shrikrishan.Pojo.ImagesData;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by mann on 29/1/18.
 */

class ImagesAdapter extends RecyclerView.Adapter<ImagesAdapter.MyViewHolder> {
    FragmentTransaction ft;
    private List<ImagesData> imagesList;
    Context mContext;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView images,shareIcon;
        TextView setAsWallpaper;
        LinearLayout mainLay;
        private TextAdapter.ItemClickListener clickListener;
        public MyViewHolder(View view) {
            super(view);
            images = (ImageView) view.findViewById(R.id.images);
            mainLay=(LinearLayout)view.findViewById(R.id.mainLay);
           // view.setOnClickListener(this);
          //  setAsWallpaper=(TextView) view.findViewById(R.id.setAsWallpaper);
        }


        public void setClickListener(TextAdapter.ItemClickListener itemClickListener) {
            this.clickListener = itemClickListener;
        }
    }


    public ImagesAdapter(List<ImagesData> imagesList, Context context, FragmentTransaction ft) {
        this.imagesList = imagesList;
        this.mContext = context;
        this.ft=ft;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.movie_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        String images=imagesList.get(position).getThumbnal();
        Picasso.with(mContext)
                .load(images)
                .into(holder.images);
holder.mainLay.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Intent i=new Intent(mContext,FullScrenActivity.class);
        i.putExtra("thumbnail",imagesList.get(position).getThumbnal());
        i.putExtra("full",imagesList.get(position).getFullImage());
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(i);
    }
});


    }

    @Override
    public int getItemCount() {
        return imagesList.size();
    }
}